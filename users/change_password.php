<?php
include_once('./check_user.php');
include_once('../conn.php');

$newPassword = password_hash($_REQUEST['password'], PASSWORD_DEFAULT);

$stmt = $conn->prepare("UPDATE users SET pass_hash = :pass_hash WHERE user = :user");

try {
    $stmt->execute(array(':pass_hash' => $newPassword, ':user' => $_SESSION['user']));
    echo json_encode(['success' => 'usuário alterado']);
} catch (Exception $e) {
    http_response_code(500);
    echo json_encode(['error' => $e]);
}
