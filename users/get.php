<?php
include_once('./check_user.php');
include_once('../conn.php');

$user = $_POST['user'];
$password = password_hash($_POST['password'], PASSWORD_DEFAULT);

$query = 'SELECT id, user, active FROM users;';
$stmt = $conn->prepare($query);

try {
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode(['success' => $result]);
} catch (Exception $e) {
    echo json_encode(['error' => $e]);
}