<?php
include_once('./check_user.php');
include_once('../conn.php');

$body = json_decode(file_get_contents("php://input"), true);

$id = intval($body['id']);
$active = intval($body['active']);

$query = "UPDATE users SET active = :active WHERE id = :id";
$stmt = $conn->prepare($query);

try {
  $stmt->execute(array(':active' => $active, ':id' => $id));
  echo json_encode(['success' => 'usuário alterado']);
} catch (Exception $e) {
  http_response_code(500);
  echo json_encode(['error' => $e]);
}