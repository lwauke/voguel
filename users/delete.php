<?php
include_once('./check_user.php');
include_once('../conn.php');

$id = $_REQUEST['id'];

$query = 'DELETE FROM users WHERE id = :id';
$stmt = $conn->prepare($query);

try {
    $stmt->execute(array(':id' => $id));
    echo json_encode(['success' => 'usuário deletado com sucesso']);
} catch (Exception $e) {
    echo json_encode(['error' => $e]);
}