<?php
include_once('./check_user.php');
include_once('../conn.php');

$user = $_POST['user'];
$password = password_hash($_POST['password'], PASSWORD_DEFAULT);

$query = 'INSERT INTO users VALUES(DEFAULT, :user, :pass, 0)';
$stmt = $conn->prepare($query);

try {
    $stmt->execute(array(':user' => $user, ':pass' => $password));
    echo 'usuário inserido com sucesso';
} catch (Exception $e) {
    echo json_encode(['error' => $e]);
}