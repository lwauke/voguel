(function(doc) {
    const passwords = doc.querySelectorAll('input[type="password"]');
    const btnSend = doc.querySelector('.btn-send');
    const msg = doc.querySelector('.msg');

    btnSend.addEventListener('click', async e => {
        e.preventDefault();

        const [password, confirmation] = [].map.call(passwords, e => e.value);

        if (password !== confirmation) {
            msg.textContent = 'Senhas não coincidem';
            return;
        }

        fetch(`./users/change_password.php?password=${password}`)
        .then(() => {
            msg.textContent = 'Usuário alterado'
        })
        .catch(() => {
            msg.textContent = 'erro ao alterar senha'
        })
    })
})(document);