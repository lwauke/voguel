(function() {
    const doc = document;
    const datesInput = Array.from(doc.querySelectorAll('.date-value'))
    const filterBtn = doc.querySelector('.filter-date')
    const graphImgs = doc.querySelectorAll('.graph-img');

    graphImgs.forEach(g => g.addEventListener('load', e => {
        e.currentTarget.parentElement.classList.remove('loading');
    }))

    filterBtn.addEventListener('click', () => {
        graphImgs.forEach(g => {
            const [initialDate, finalDate] = datesInput.map(({value: v}) => v);
            g.parentElement.classList.add('loading');
            g.src = `${g.dataset.endpoint}&from=${initialDate}&to=${finalDate}`
        })
    })
}());
