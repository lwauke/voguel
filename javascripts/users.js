(function(){
    const doc = document
    function fetchUsers() {
        fetch('./users/get.php')
        .then(r => r.json())
        .then(({ success }) => {
            const htmlList = success.reduce((acc, {id, user, active}) => `
                ${acc}
                <tr data-id=${id}>
                    <td>${id}</td>
                    <td>${user}</td>
                    <td>
                        <input class="toggle-active-user" type="checkbox" ${!!parseInt(active) ? 'checked' : 'unchecked'}="" />
                    </td>
                    <td>
                        <button class="delete-user">deletar</button>
                    </td>
                </tr>
            `, '');
            doc.querySelector('.users-list').innerHTML = htmlList;
        })
        .catch(console.log)
        .then(addDeleteListener)
        .then(addUpdateUser)
    };

    const grandFather = ({ parentElement: { parentElement: target } }) => target;
    const msg = doc.querySelector('.message');
                    
    function addUpdateUser() {
        doc.querySelectorAll('.toggle-active-user').forEach(b => 
            b.addEventListener('input', ({ currentTarget }) => {
                const { dataset: { id } } = grandFather(currentTarget)
                const active = +currentTarget.checked
                
                fetch('./users/update.php', {
                    method: 'PUT',
                    body: JSON.stringify({ id, active }),
                    headers: new Headers({"Content-type": "application/x-www-form-urlencoded"})
                })
                .then(r => {
                    msg.classList.remove('hidden')
                    return r.json()
                })
                .then(({ success }) => {
                    msg.innerHTML = success;                    
                })
                .catch(({ error }) => {
                    console.log(error)
                    msg.innerHTML = 'Erro ao alterar';    
                })

                fetchUsers();
            })
        )
    }

    function addDeleteListener() {
        doc.querySelectorAll('.delete-user').forEach(b => {
            b.addEventListener('click', ({ currentTarget }) => {
                const { dataset: { id } } = grandFather(currentTarget)
                fetch('./users/delete.php?id=' + id)
                .then(r => r.json())
                .then(({ success }) => {
                    msg.classList.remove('hidden');
                    msg.innerHTML = success;
                    fetchUsers();
                })
            })
        })
    }

    fetchUsers();
}());