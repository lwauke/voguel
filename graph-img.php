<?php
error_reporting(E_ALL);
set_time_limit(1800);

include_once('./check_status.php'); 
include_once('./helper-api.php'); 
include_once('./z_user.php'); 


$version = curlRequest('apiinfo.version', [], null);

$isVersion3 = preg_match('/^3/', $version);

function ImageById ($url, $params) {
    global $z_server, $z_user, $z_password;
    $z_url_index   = $z_server ."/index.php";
    $z_login_data  = array('name' => $z_user, 'password' => $z_password, 'enter' => "Sign in");

    // file names
    $filename_cookie = tempnam(".", "zabbix_cookie");

    //setup curl
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $z_url_index);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $z_login_data);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $filename_cookie);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $filename_cookie);
    // login
    curl_exec($ch);
    // get graph
    curl_setopt($ch, CURLOPT_URL, $z_server . '/' . $url ."?" . $params);
    $output = curl_exec($ch);
    curl_close($ch);
    // delete cookie
    header("Content-type: image/png");
    unlink($filename_cookie);
    return $output;
}

 // FUNCTION
function GraphImageById ($graphid, $period = 3600, $width = 400, $height = 200, $from = null, $to = null) {
   global $version, $isVersion3;
     $params = array(
        'graphid' => $graphid,
        'width' => $width,
        'height' => $height,
        'period' => $period,
     );

     if(!is_null($from) && !is_null($to)) {
      $params = $isVersion3
         ? array_merge(
            $params,
            array(
               'isNow' => 0,
               'period' => strtotime($to) - strtotime($from),
               'stime' => strtotime($from)
            )
         )
         : array_merge(
            $params,
            array(
               'from' => $from,
               'to' => $to,
               'profileIdx' => 'web.graphs.filter'
            )
         );
     }
     return ImageById('chart2.php', http_build_query($params));
}

function toNullable($ref) {
   $ref = isset($ref) || !empty($ref) ? $ref : null;
}

$graphid = $_REQUEST['graphid'];
$mapid = toNullable($_REQUEST['mapid']);
$width = toNullable($_REQUEST['width']);
$height = toNullable($_REQUEST['height']);
$period = toNullable($_REQUEST['period']);
$from = $_REQUEST['from'];
$to = $_REQUEST['to'];

echo GraphImageById($graphid,$period,$width,$height, $from, $to);
?>