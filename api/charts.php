<?php
require_once __DIR__ . '/../helper-api.php';
require_once __DIR__ . '/../check-status.php';

$token = curlRequest(
    'user.login',
    array(
        'user' => 'apizabbix',
        'password' => 'co2FdxM8#'
    ),
    null
);

$graph_data = curlRequest(
    'item.get',
    array(
        'output' => [ "key_", "name", "lastvalue", "itemid"],
        "selectHosts" => [ "name", "hostid" ],
        'search' => (object) ["name" => [$_SESSION['user'], "Operational status"]],
        'excludeSearch' => false,
    ),
    $token
);

$response = array();

foreach($graph_data as $gd) {
    $hosts = $gd['hosts'][0];
    $alis_temp = explode('.', $gd['key_'])[3];
    $result = curlRequest(
        'item.get',
        array(
            'output' => ['graphs'],
            "hostids" => $hosts['hostid'],
            "selectHosts" => [ "name", "hostid" ],
            'search' => (object) ['key_' => "net.if.in[ifHCInOctets." . $alis_temp],
            "selectGraphs" => ['graphid']
        ),
        $token
    );                

    $graphid = $result[0]['graphs'][0]['graphid'];
    $lastvalue = $gd['lastvalue'];

    array_push(
        $response,
        array(
            'graphid' => $graphid,
            'lastvalue' => $lastvalue
        )
    );
}

echo json_encode($response);

?>