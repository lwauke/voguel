<header>
    <a href="./dashboard.php">
        <img src="./img/logo.png" class="logo" />
    </a>
    <div class="user-nav">
        <div class="user-icon"></div>
        <nav>
            <ul>
                <?php
                    if($_SESSION['user'] === 'admin') {
                        echo "<li>
                            <a href='./users.php'>usuários</a>
                        </li>";
                    }
                ?>
                <li>
                    <a href="./profile.php">perfil</a>
                </li>      
                <li>
                    <a href="./logout.php">logout</a>
                </li>
            </ul>
        </nav>    
    </div>
</header>