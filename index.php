<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./stylesheets/login.css">
    <title>Login - Portal Voguel</title>
</head>
<body>
    <img src="./img/logo.png" class="logo">
    <form method="POST" action="./login.php">
        <input type="text" placeholder="usuário" name="user" id="user">
        <input type="password" placeholder="senha" name="password" id="password">
        <input type="submit" value="login">
    </form>
</body>
</html>
