<?php include_once('./check_status.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./stylesheets/style.css"> 
    <link rel="stylesheet" href="./stylesheets/profile.css"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> 
    <title>Dashboard - Vogel</title>
</head>
<body>
    <?php include_once('./header.php'); ?>
    <main>
       <h2 class="main-title">Alterar senha</h2>
       <form>
           <label>Senha:</label>
           <input type="password" />
           <label>Confirme a senha:</label>
           <input type="password" />
            <button class="btn-send">Resetar</button>
       </form>
       <p class="msg"></p>
    </main>
    <footer></footer>
    <script src="./javascripts/profile.js"></script>
</body>
</html>