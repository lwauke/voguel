<?php
include_once('./conn.php');

function checkPassword($user, $password) {
    global $conn;

    $query = 'SELECT pass_hash FROM users WHERE user = :user';
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':user' => $user));

    return password_verify(
        $password,
        $stmt->fetchAll()[0]['pass_hash']
    );
}

function isActive($user) {
    global $conn;

    $query = 'SELECT active FROM users WHERE user = :user';
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':user' => $user));

    return intval($stmt->fetchAll()[0]['active']);
}

$isAuthenticated = checkPassword(
    $_POST['user'],
    $_POST['password']
);

if($isAuthenticated) {
    if (!boolval(isActive($_POST['user']))) {
        header('location:index.php');
    } else {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_cache_expire(180);
            session_start();        
        }
        $_SESSION['logged'] = true;
        $_SESSION['user'] = $_POST['user'];
        header('location:dashboard.php');
    }
} else {
    header('location:index.php');
}