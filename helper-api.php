<?php
include_once('./check_status.php'); 
include_once('./z_user.php'); 

function curlRequest($method, $params, $auth = null) {
  global $z_server;
  $newParams = array(
    'jsonrpc' => '2.0',
    'method' => $method,
    'params' => $params,
    'id' => 1
  );

  if(!is_null($auth)) {
    $newParams = array_merge($newParams, array('auth' => $auth));
  }

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, "$z_server/api_jsonrpc.php");
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($newParams)); 

  $response = curl_exec($ch);

  curl_close($ch);

  return json_decode($response, true)['result'];
}

function get_graphs($name) {
    global $z_user, $z_password;
    $token = curlRequest(
        'user.login',
        array(
            'user' => $z_user,
            'password' => $z_password
        ),
        null
    );
    
    $graphs = curlRequest(
        'graph.get',
        array(
            'output' => ['name', 'graphid'],
            'search' => (object) ["name" => $name],
            'excludeSearch' => false
        ),
        $token
    );
    
    $token = curlRequest(
        'user.logout',
        array(),
        $token
    );   

    return $graphs;
}

function get_status($name) {
    global $z_user, $z_password;
    $token = curlRequest(
        'user.login',
        array(
            'user' => $z_user,
            'password' => $z_password
        ),
        null
    );
    
    $graphs = curlRequest(
        'item.get',
        array(
            'output' => ['name', 'lastvalue'],
            'search' => (object) ["name" => $name],
            'excludeSearch' => false,
            'monitored' => true
        ),
        $token
    );
    
    $token = curlRequest(
        'user.logout',
        array(),
        $token
    );   

    return $graphs;
}