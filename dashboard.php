<?php include_once('./check_status.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./stylesheets/style.css"> 
    <link rel="stylesheet" href="./stylesheets/dashboard.css"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> 
    <title>Dashboard - Vogel</title>
</head>
<body>
    <?php include_once('./header.php'); ?>
    <main>
       <h1 class="main-title">Dashboard - Zabbix</h1>
       <div class='subtitle'><h2>Portas</h2></div>
       <div class="subtitle">
            <h2>Gráficos</h2>
            <div class="filter">
                <label for="">Data inicial:</label>
                <input type="date" class="initial-date date-value"/>
                <label for="">Data final:</label>
                <input type="date" class="final-date date-value"/>
                <button class="filter-date">Filtrar</button>
            </div>
        </div>
        <?php
            require_once __DIR__ . '/helper-api.php';
            require_once __DIR__ . '/z_user.php';

            $token = curlRequest(
                'user.login',
                array(
                    'user' => $z_user,
                    'password' => $z_password
                ),
                null
            );
            
            $graph_data = curlRequest(
                'item.get',
                array(
                    'output' => [ "key_", "name", "lastvalue", "itemid"],
                    "selectHosts" => [ "name", "hostid" ],
                    'search' => (object) ["name" => [$_SESSION['user'], "Operational status"]],
                    'excludeSearch' => false,
                ),
                $token
            );            
            foreach($graph_data as $gd) {
                $hosts = $gd['hosts'][0];
                $alis_temp = explode('.', $gd['key_'])[3];
                $result = curlRequest(
                    'item.get',
                    array(
                        'output' => ['graphs'],
                        "hostids" => $hosts['hostid'],
                        "selectHosts" => [ "name", "hostid" ],
                        'search' => (object) ['key_' => "net.if.in[ifHCInOctets." . $alis_temp],
                        "selectGraphs" => ['graphid']
                    ),
                    $token
                );                

                $graphid = $result[0]['graphs'][0]['graphid'];
                $status = $gd['lastvalue'];
                
                $classStatus = intval($status) === 1 ? 'ok' : 'not-ok';
                $endpoint = "./graph-img.php?graphid=" . $graphid;

                echo "
                    <div class='status-container $classStatus'></div>
                    <div class='graph-img-container'>
                        <img class='graph-img' src=\"$endpoint\" data-endpoint=\"$endpoint\"/>
                    </div>
                ";
            }
            
            curlRequest(
                'user.logout',
                array(),
                $token
            );
        ?>
    </main>
    <footer></footer>
    <script src="./javascripts/dashboard.js"></script>
</body>
</html>