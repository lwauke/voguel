<?php
if (session_status() !== PHP_SESSION_ACTIVE)
    session_start();

if(!isset($_SESSION['logged']) || !isset($_SESSION['user']))
    header('location:index.php');
?>