<?php include_once('./check_status.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./stylesheets/style.css">    
    <link rel="stylesheet" href="./stylesheets/users.css">    
    <title>Dashboard - Vogel</title>
</head>
<body>
    <?php
        include_once('./header.php');
        if($_SESSION['user'] !== 'admin') {
            header('location:dashboard.php');
        }
    ?>
    <main>
        <div class="container-table">
            <h3>Users</h3>
            <table>
                <thead>
                    <th>id</th>
                    <th>user</th>
                    <th>active</th>
                    <th></th>
                </thead>
                <tbody class="users-list"></tbody>
            </table>
            <p class="message hidden"></p>
        </div>
        <form method="POST" action="./users/insert.php">
            <h3>Inserir</h3>
            <input type="text" placeholder="usuário" name="user" id="user">
            <input type="password" placeholder="senha" name="password" id="password">
            <input type="submit" value="inserir">
        </form>
    </main>
    <footer></footer>
    <script src="./javascripts/users.js"></script>
</body>
</html>